<?php

namespace App\Http\Controllers;

use App\Service\ThreadManager;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    private $manager = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->manager = new ThreadManager();
    }

    public function index()
    {
        $threads = $this->manager->findAll();

        return response()->json($threads);
    }

    public function show($label)
    {
        try {
            $thread = $this->manager->getWithComments($label);
            $thread->addVisible('comments');

            return response()->json($thread);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('thread_index');
        }
    }

    public function addComment(Request $request, $label)
    {
        $comment = $request->input('comment', 'Someone commented ...');

        if ($this->manager->hasThread($label) === false) {
            return redirect()->route('thread_index');
        }

        try {
            DB::beginTransaction();

            $this->manager->addComment($label, $comment);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return redirect()->route('thread_show', ['label' => $label]);
    }
}
