<?php

namespace App\Http\Controllers;

use App\Service\ThreadManager;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $contents = [
            'thread' => [
                'method' => 'GET',
                'uri' => route('thread_index'),
            ],
        ];

        return response()->json($contents);
    }
}
