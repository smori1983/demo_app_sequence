<?php

namespace App\Service;

use App\Model\Thread;
use App\Model\ThreadComment;
use App\Model\ThreadSequence;
use DB;

class ThreadManager
{
    public function findAll()
    {
        $query = Thread::query();
        $query->orderBy('last_commented_at', 'DESC');
        $query->orderBy('label', 'ASC');

        return $query->get();
    }

    public function get($label)
    {
        $query = Thread::query();
        $query->where('label', $label);

        return $query->first();
    }

    public function getWithComments($label)
    {
        $query = Thread::query();
        $query->where('label', $label);
        $query->with(['comments' => function ($query) {
            $query->limit(5);
        }]);

        return $query->firstOrFail();
    }

    public function hasThread($label)
    {
        $query = Thread::query();
        $query->where('label', $label);

        return $query->first() !== null;
    }

    public function add(array $data)
    {
        $thread = new Thread();
        $thread->label = $data['label'];
        $thread->title = $data['title'];
        $thread->last_commented_at = Thread::DEFAULT_LAST_COMMENTED_AT;
        $thread->save();

        $sequence = new ThreadSequence();
        $sequence->thread_id = $thread->id;
        $sequence->save();
    }

    public function addComment($label, $comment)
    {
        if ($this->hasThread($label) === false) {
            return;
        }

        $thread = $this->get($label);

        $threadComment = new ThreadComment();
        $threadComment->sequence = $this->nextSequenceValue($thread);
        $threadComment->comment = $comment;

        $thread->comments()->save($threadComment);

        $thread->last_commented_at = $threadComment->created_at;
        $thread->save();
    }

    private function nextSequenceValue(Thread $thread)
    {
        $query = DB::table('thread_sequence');
        $query->where('thread_id', $thread->id);
        $query->lockForUpdate();

        $row = $query->first();

        $result = $row->comment + 1;

        $query = DB::table('thread_sequence');
        $query->where('thread_id', $thread->id);
        $query->update(['comment' => $result]);

        return $result;
    }
}
