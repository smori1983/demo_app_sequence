<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ThreadComment extends Model
{
    protected $table = 'thread_comment';

    protected $visible = [
        'sequence',
        'comment',
    ];
}
