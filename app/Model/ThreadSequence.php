<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ThreadSequence extends Model
{
    protected $table = 'thread_sequence';

    public $timestamps = false;
}
