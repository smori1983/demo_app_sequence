<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    const DEFAULT_LAST_COMMENTED_AT = '2000-01-01 00:00:00';

    protected $table = 'thread';

    protected $visible = [
        'label',
        'title',
        'links',
    ];

    protected $appends = [
        'links',
    ];

    public function comments()
    {
        return $this
            ->hasMany(ThreadComment::class, 'thread_id', 'id')
            ->orderBy('sequence', 'DESC');
    }

    public function getLinksAttribute()
    {
        return [
            'show' => [
                'method' => 'GET',
                'uri' => route('thread_show', ['label' => $this->label]),
            ],
            'comment' => [
                'method' => 'POST',
                'uri' => route('thread_comment_add', ['label' => $this->label]),
            ],
        ];
    }
}
