<?php

use Illuminate\Database\Seeder;

use App\Service\ThreadManager;

class SampleThreadSeeder extends Seeder
{
    public function run()
    {
        $threads = [
            ['label' => 'java', 'title' => 'Java'],
            ['label' => 'php', 'title' => 'PHP'],
            ['label' => 'python', 'title' => 'Python'],
            ['label' => 'ruby', 'title' => 'Ruby'],
        ];

        $manager = new ThreadManager();

        DB::beginTransaction();

        foreach ($threads as $threadData) {
            if ($manager->hasThread($threadData['label'])) {
                continue;
            }

            $manager->add($threadData);
        }

        DB::commit();
    }
}
