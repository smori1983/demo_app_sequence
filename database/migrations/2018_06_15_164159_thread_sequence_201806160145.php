<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreadSequence201806160145 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thread_sequence', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigInteger('thread_id')->unsigned();
            $table->bigInteger('comment')->unsigned()->default(0);

            $table->primary('thread_id');

            $table->foreign('thread_id')->references('id')->on('thread');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('thread_sequence');
    }
}
