<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Thread201806160141 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thread', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->string('label', 100);
            $table->string('title', 100);
            $table->dateTime('last_commented_at');
            $table->timestamps();

            $table->unique('label', 'uq_thread_code');
            $table->index(['last_commented_at', 'label'], 'idx_thread_last_commented_at_label');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('thread');
    }
}
