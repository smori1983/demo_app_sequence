<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThreadComment201806160141 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thread_comment', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('thread_id')->unsigned();
            $table->bigInteger('sequence')->unsigned();
            $table->string('comment', 500);
            $table->timestamps();

            $table->unique(['thread_id', 'sequence'], 'uq_thread_comment_thread_id_sequence');

            $table->foreign('thread_id')->references('id')->on('thread');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('thread_comment');
    }
}
