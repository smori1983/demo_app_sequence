<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', [
    'as' => 'index',
    'uses' => 'IndexController@index',
]);

$router->get('/thread', [
    'as' => 'thread_index',
    'uses' => 'ThreadController@index',
]);

$router->get('/thread/{label}', [
    'as' => 'thread_show',
    'uses' => 'ThreadController@show',
]);

$router->post('/thread/{label}/comment', [
    'as' => 'thread_comment_add',
    'uses' => 'ThreadController@addComment',
]);
